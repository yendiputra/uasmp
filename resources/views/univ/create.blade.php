@extends('layout.master')
@section('judul')

@endsection

@push('script')
<script src="admin/plugins/datatables/jquery.dataTables.js"></script>
<script src="admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')

<h2>Tambah Data</h2>
        <form action="/univ" method="POST">
            @csrf
            <div class="form-group">
                <label for="nama_univ">nama_univ</label>
                <input type="text" class="form-control" name="nama_univ" id="nama_univ" placeholder="Masukkan nama_univ">
                @error('nama_univ')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="alamat_univ">alamat_univ</label>
                <input type="text" class="form-control" name="alamat_univ" id="alamat_univ" placeholder="Masukkan alamat_univ">
                @error('alamat_univ')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
        @endsection