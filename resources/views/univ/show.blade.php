@extends('layout.master')
@section('judul')
Data Tables
@endsection

@push('script')
<script src="admin/plugins/datatables/jquery.dataTables.js"></script>
<script src="admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')

<h2>Data Universitas</h2>
<table class="table">
    <tr>
        <td>id</td>
        <td>:</td>
        <td> {{$univ->id}}</td>
    </tr>
    <tr>
        <td>Nama Universitas</td>
        <td>:</td>
        <td> {{$univ->nama_univ}}</td>
    </tr>
    <tr>
        <td>Alamat Universitas</td>
        <td>:</td>
        <td> {{$univ->alamat_univ}}</td>
    </tr>
</table>
<a href="/univ" class="btn btn-primary mb-3">Kembali</a>
@endsection