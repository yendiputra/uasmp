<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Univ;

class UnivController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return view('univ.index');
        $univ= Univ::all();
        return view('univ.index', compact('univ'));
 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('univ.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$this->validate($request,[
    		'nama_univ' => 'required',
    		'alamat_univ' => 'required'
    	]);
 
        Univ::create([
    		'nama_univ' => $request->nama_univ,
    		'alamat_univ' => $request->alamat_univ
    	]);
 
    	return redirect('/univ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $univ= Univ::find($id);
        return view('univ.show', compact('univ'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $univ = Univ::find($id);
        return view('univ.edit', compact('univ'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_univ' => 'required|unique:univ',
            'alamat_univ' => 'required',
        ]);

        $univ = Univ::find($id);
        $univ->nama_univ = $request->nama_univ;
        $univ->alamat_univ = $request->alamat_univ;
        $univ->update();
        return redirect('/univ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $univ = Univ::find($id);
        $univ->delete();
        return redirect('/univ');
    }
}
