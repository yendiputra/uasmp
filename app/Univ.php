<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Univ extends Model
{
    protected $table = "univ";
    protected $fillable = ["nama_univ", "alamat_univ"];
}
